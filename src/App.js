import React, { Component } from 'react';
import Header from './Header';
// import Body from './Body';
import Footer from './Footer';
import Home from './components/home';
import Account from './components/account'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
class App extends Component {
  // constructor(props) {
  //   super(props)

  // }
    render() {
        return (
          <Router>
            <div>
              <Header/>
                {/* {this.props.children} */}
              <Footer />
                <Switch>
                  <Route exact path="/">
                    <Home />
                  {/* <Route path="/home">
                    <Home /> */}
                  </Route>
                  <Route path="/account">
                    <Account />
                  </Route>
                </Switch>
              </div>
          </Router>
        )
        }
    }
    export default App;