  
import React from 'react';
import Home from './components/Home';

const routes = [
    {
        path : '/',
        exact : true,
        main : () => <Home />
    },
    {
        path : './about',
        exact : false,
        main : () => <About />
    },
    {
        path : './contact',
        exact : false,
        main : () => <Contact />
    },
    {
        path : './products',
        exact : false,
        main : ({ match, location }) => <Products match={match} location={location} />
    },
    {
        path : './Login',
        exact : false,
        main : ({location}) => <Login location={location} />
    }
];

export default routes;